﻿public class Program
{

    public static void Main(string[] args)
    {

        int[] numbers = new int[10];
        Random rnd = new Random();
        for (int i = 0; i < 10; i++)
        {
            numbers[i] = rnd.Next(1000);
        }

        Console.WriteLine("Unsorted Array:");
        printArray(numbers);

        Console.WriteLine("Sorted Array:");
        sortedArray(numbers);

    }

    public static int[] sortedArray(int[] numbers)
    {

        sortMerge(numbers);
        printArray(numbers);
        
        return numbers;
    }


    public static void printArray(int[] numbers)
    {
        for (int i = 0; i < numbers.Length; i++)
        {
            Console.WriteLine(numbers[i]);
        }
    }

    public static void sortMerge(int[] inputNumbers)
    {
       if(inputNumbers.Length < 2)
        {
            return;
        }

        int indexMiddle = inputNumbers.Length / 2;
        int[] leftArray = new int[indexMiddle];
        int[] rightArray = new int[inputNumbers.Length - indexMiddle];

        for(int i = 0; i < indexMiddle; i++) {
            leftArray[i] = inputNumbers[i];
        }

        int j = 0;
        for (int i = indexMiddle; i < inputNumbers.Length; i++)
        {
            rightArray[j] = inputNumbers[i];
            j++;
        }

        sortMerge(leftArray);
        sortMerge(rightArray);

        merge(inputNumbers, leftArray, rightArray);

    }

    public static void merge(int[] inputArray, int[] leftArray, int[] rightArray)
    {
        int i = 0;
        int j = 0;
        int k = 0;

        while(i < leftArray.Length && j < rightArray.Length)
        {
            if(leftArray[i] < rightArray[j])
            {
                inputArray[k] = leftArray[i];
                i++;
                k++;
            } else
            {
                inputArray[k] = rightArray[j];
                j++;
                k++;
            }
        }

        while (i < leftArray.Length)
        {
            inputArray[k] = leftArray[i];
            i++;
            k++;
        }

        while (j < rightArray.Length)
        {
            inputArray[k] = rightArray[j];
            j++;
            k++;
        }

    }

}