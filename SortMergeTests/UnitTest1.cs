using Xunit.Sdk;

namespace SortMergeTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void testSortedArray()
        {
            int[] input = { 10, 15, 5, 0, 1, 13, 21, 18, 24 };
            int[] expected = { 0, 1, 5, 10, 13, 15, 18, 21, 24 };
            int [] result = Program.sortedArray(input);
            CollectionAssert.AreEqual(result, expected);
        }
     
    }

}
